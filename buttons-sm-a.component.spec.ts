import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonsSmAComponent } from './buttons-sm-a.component';

describe('ButtonsSmAComponent', () => {
  let component: ButtonsSmAComponent;
  let fixture: ComponentFixture<ButtonsSmAComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonsSmAComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonsSmAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
